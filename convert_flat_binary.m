function medianTrace = convert_flat_binary(filename, nChansTotal,chidx,channel_order,outputDir)
% Subtracts median of each channel, then subtracts median of each time
% point.
%
% filename should include the extension
% outputDir is optional, by default will write to the directory of the input file
%
% should make chunk size as big as possible so that the medians of the
% channels differ little from chunk to chunk.
chunkSize = 1000000;

fid = []; fidOut = [];

d = dir(filename);
nSampsTotal = d.bytes/nChansTotal/2;
% nChunksTotal = 2;
nChunksTotal = ceil(nSampsTotal/chunkSize);
try
    
    [pathstr, name, ext] = fileparts(filename);
    fid = fopen(filename, 'r');
    if nargin < 3
        outputFilename  = [pathstr filesep name '_CAR' ext];
        mdTraceFilename = [pathstr filesep name '_medianTrace.mat'];
    else
        outputFilename  = [outputDir filesep name '_CAR' ext];
        mdTraceFilename = [outputDir filesep name '_medianTrace.mat'];
    end
    fidOut = fopen(outputFilename, 'w');

    chunkInd = 1;
%     keyboard
    medianTrace = zeros(1, nSampsTotal);
    while 1
        
        fprintf(1, 'chunk %d/%d\n', chunkInd, nChunksTotal);
        
        dat = fread(fid, [nChansTotal chunkSize], '*int16');
        
        if ~isempty(dat)
            tmp = dat(chidx,:);
            
            tmp = bsxfun(@minus, tmp, median(tmp,2)); % subtract median of each channel
            tm = median(tmp,1);
            tmp =bsxfun(@minus, tmp, tm); % subtract median of each time point
            n_ch = length(chidx);
%             keyboard
            
            fwrite(fidOut, tmp([channel_order(chidx)+1],:), 'int16');
            medianTrace((chunkInd-1)*chunkSize+1:(chunkInd-1)*chunkSize+numel(tm)) = tm;
            
        else
            break
        end
        
        chunkInd = chunkInd+1;
    end
    
    save(mdTraceFilename, 'medianTrace', '-v7.3');
    fclose(fid);
    fclose(fidOut);
    
catch me
    
    if ~isempty(fid)
        fclose(fid);
    end
    
    if ~isempty(fidOut)
        fclose(fidOut);
    end
    
    
    rethrow(me)
    
end
