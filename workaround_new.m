clear all
close all
clc



filedir = 'E:\data';
cd(filedir)


channel_file = 'E:\code\cvb_repo\configs\atlas_oe_hs_4sh.channelmap';

%% Load chanelmap file

f = fopen(fullfile(channel_file), 'r');
C = textscan(f, '%f%f%f%f%f%f', 'Delimiter', ',','headerlines',1);
fclose(f);
%%

nchan = length(C{1});
chkeep = ~C{5};
chorder = C{2};

%%
raw_folder = 'raw_oe';
tmp = list_files([raw_folder],'continuous.dat');

%%
% for ii=length(tmp):-1:length(tmp)-1
%     for ii=1:length(tmp)
    ii = 2
    tt = strsplit(tmp{ii},'\');
    sname = sprintf('%s\\%s\\',tt{1},tt{2});
    medianTrace = convert_flat_binary(tmp{ii},nchan,chkeep,chorder,sname);
    %keyboard
% medianTrace = convert_flat_binary(filename,nchan,chkeep,chorder,filedir);
% end






%%

filedir = 'E:\local\users\cagatay\open_ephys';
cd(filedir)

%% load channelmap

channel_file = 'test.channelmap';

%%


f = fopen(channel_file, 'r');
C = textscan(f, '%f%f%f%f%f%f', 'Delimiter', ',','headerlines',1);
fclose(f);

%%

nchan = length(C{1});
chkeep = ~C{5};
chorder = C{2};

filename = '190724_MB096.bin';

medianTrace = convert_flat_binary(filename,nchan,chkeep,chorder,filedir);

%%

