clear all
close all
clc

addpath(genpath('D:\haeslerlab\code\3rd Party\npy-matlab-master'))

% filedir = 'E:\data';
filedir = 'E:\local\users\cagatay\PTSD\';
cd(filedir)


%%

cd E:\local\users\cagatay\PTSD\sort\190904_CVB003

temps = readNPY(fullfile(pwd, 'templates.npy'));

chmap = readNPY(fullfile(pwd, 'channel_map.npy'));

chpos = readNPY(fullfile(pwd, 'channel_positions.npy'));

clf,
plot(chpos(:,1),chpos(:,2),'o')

%%

clf,

ic = 12;

% tt= [12,48,73,101,102,109]
nchannels = length(chmap);
nsamples = size(temps,2);

tw = 0:(nsamples-1);
offset = 0;


for ii = 1:nchannels
    
    plot(offset+tw+chpos(ii,1)+40,squeeze(temps(ic,:,ii))*50+chpos(ii,2)), hold on
%     keyboard
    
end

