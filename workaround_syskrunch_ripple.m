clear all
close all
clc



% filedir = 'E:\data';
filedir = 'E:\local\users\cagatay\PTSD\';
cd(filedir)


channel_file = 'D:\haeslerlab\code\user\cagatay\cvb_repo\configs\atlas_oe_hs_4sh.channelmap';

%% Load chanelmap file

f = fopen(fullfile(channel_file), 'r');
C = textscan(f, '%f%f%f%f%f%f', 'Delimiter', ',','headerlines',1);
fclose(f);
%%

nchan = length(C{1});
chkeep = ~C{5};
chorder = C{2};

%%
raw_folder = 'data';
tmp = list_files([raw_folder],'continuous.dat')


%%
nch = 43;
srate = 30000;

out = load_binary_file(tmp{1},43,[],[],1,true);

% dat = zeros(sum(chkeep),length(out.Data.data));
% dat = double(out.Data.data(chkeep,:));
% ttmp = zeros(size(out.Data.data))
dat = zeros(sum(chkeep),length(out.Data.data));
dat = double(out.Data.data(chorder(chkeep)+1,:));

% dat = zeros(sum(chkeep),size(ttmp,2));

%% lowpass filter
[b,a]=butter(3,[600]*2/(srate),'low');
lfp = dat;
dat = dat - repmat(median(dat,1),size(lfp,1),1);
disp('Loaded data and subtracted median.')
for i = 1:size(dat,1)
    lfp(i,:)=filtfilt(b,a,dat(i,:));
end

%% downsample
dsrate = 1250;
dlfp = lfp(:,1:srate/dsrate:end);
%dlfp = dlfp - median(dlfp,1);
time = (0:(length(dlfp)-1))/dsrate;
disp('Done filtering lfp and downsampling.')


%% whiten lfp
for ch = 1:size(dlfp,1)
    nlfp(:,ch) = whitenlfp(dlfp(ch,:),1250*2000);
end


%% find the best channel to detect sharp wave ripples

used = dlfp;
passband = [140,250];
nyquist = dsrate/2;
[b a] = butter(4,[passband(1)/nyquist passband(2)/nyquist],'bandpass');
for ii = 1:size(used,1)
    filt = FiltFiltM(b,a,single(used(ii,:)));
    pow = fastrms(filt,15);    
    mRipple(ii) = mean(pow);
    meRipple(ii) = median(pow);
    mmRippleRatio(ii) = mRipple(ii)./meRipple(ii);
end

mmRippleRatio(mRipple<1) = 0;
mmRippleRatio(meRipple<1) = 0;

[minVal loc] = max(mmRippleRatio);
%%
loc = 6;
signal = (used(loc,:));

rppl = bz_FindRipples(signal',time','EMGThresh',0,'saveMat',logical(1));

[maps,data,stats] = bz_RippleStats(signal',time',rppl);

%%
bz_PlotRippleStats(maps,data,stats)


%%
clf,
passband = [140,250];
nyquist = dsrate/2;
[b a] = butter(4,[passband(1)/nyquist passband(2)/nyquist],'bandpass');

% get some generic plot for the analysis
[v big_rppl]= max(rppl.peakNormedPower);

% onsets = rppl.timestamps(big_rppl,:); 
pp = rppl.peaks(big_rppl,:);
ww = 0.1;
onsets = [pp-ww pp+ww];
idx= find(time >onsets(1)& time <=onsets(2));
filt = nan(32,length(idx));
for ii = 1:size(dlfp,1)
    filt(ii,:) = FiltFiltM(b,a,single(dlfp(ii,idx)));
end

figure,
subplot(1,2,1)
tcOffsetPlot(dlfp(:,idx)')
subplot(1,2,2)
tcOffsetPlot(filt')

%%
% for ii=length(tmp):-1:length(tmp)-1
%     for ii=1:length(tmp)
    ii = 1 % for the first experiment
   
    tt = strsplit(tmp{ii},'\');
    sname = sprintf('%s\\%s\\',tt{1},tt{2});
    medianTrace = convert_flat_binary(tmp{ii},nchan,chkeep,chorder,sname);
    %keyboard
% medianTrace = convert_flat_binary(filename,nchan,chkeep,chorder,filedir);
% end






%%

filedir = 'E:\local\users\cagatay\open_ephys';
cd(filedir)

%% load channelmap

channel_file = 'test.channelmap';

%%


f = fopen(channel_file, 'r');
C = textscan(f, '%f%f%f%f%f%f', 'Delimiter', ',','headerlines',1);
fclose(f);

%%

nchan = length(C{1});
chkeep = ~C{5};
chorder = C{2};

filename = '190724_MB096.bin';

medianTrace = convert_flat_binary(filename,nchan,chkeep,chorder,filedir);

%%

