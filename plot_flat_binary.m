function plot_flat_binary(file,srate,nchannel,amp_offset,time_offset)

% file: can be .bin or .dat
% srate: sampling rate in Hz (30000)
% nchannels: number of channels in the actual recording
% time in ms 
% offset of the lines presented

if  nargin < 5 | isempty(time_offset)
    time_offset = 0.1;
 end

if nchannel>32
    nch = 32;
end

twin = time_offset+[0 0.1];
datatype = 'int16';
        
filenamestruct = dir(file);
dataTypeNBytes = numel(typecast(cast(0, datatype), 'uint8')); 
nsamp = filenamestruct.bytes/(nchannel*dataTypeNBytes);  % Number of samples per channel
mmf = memmapfile(fullfile(file), 'Format', {datatype, [nchannel nsamp], 'x'});

samps = ceil(srate*(twin));

% keyboard
datAll = double(mmf.Data.x(:,samps(1):samps(2)))';


if nargin<4 | isempty(amp_offset) 
    amp_offset = double(5*(nanstd(datAll)));
end

[nsamples,nch]=size(datAll);


offsets = repmat(0:amp_offset:(nch-1)*amp_offset,nsamples,1);

h = plot(datAll+offsets);

if nch >1 
    ylim([-amp_offset(1) amp_offset(1)*(nch)]);
end

set(h(2:end),'handlevisibility','off');
box off;

return